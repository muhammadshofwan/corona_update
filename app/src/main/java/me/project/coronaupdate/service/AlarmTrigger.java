package me.project.coronaupdate.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import androidx.legacy.content.WakefulBroadcastReceiver;

public class AlarmTrigger  extends WakefulBroadcastReceiver {
    @Override
    public void onReceive (final Context context, Intent intent) {
        Log.i("trigger", "receiveAlarm @ "+ SystemClock.elapsedRealtime());
        Intent service = new Intent(context, AlarmRecorder.class);
        startWakefulService(context, service);
        scheduleExactAlarm(context, (AlarmManager)context.getSystemService(Context.ALARM_SERVICE));

    }
    public static void scheduleExactAlarm(Context context, AlarmManager alarms) {
        Log.i("trigger", "scheduleAlarm @ "+SystemClock.elapsedRealtime());
        Intent i=new Intent(context, AlarmTrigger.class);
        PendingIntent pi= PendingIntent.getBroadcast(context, 0, i, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarms.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()+5*1000-SystemClock.elapsedRealtime()%1000, pi);
        }
    }
    public static void cancelAlarm(Context context, AlarmManager alarms) {
        Intent i=new Intent(context, AlarmTrigger.class);
        PendingIntent pi= PendingIntent.getBroadcast(context, 0, i, 0);
        alarms.cancel(pi);
    }
}
