package me.project.coronaupdate.service;

import android.app.AlarmManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class AlarmService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        AlarmTrigger.scheduleExactAlarm(AlarmService.this, (AlarmManager) getSystemService(ALARM_SERVICE));
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        AlarmTrigger.cancelAlarm(this, (AlarmManager)getSystemService(ALARM_SERVICE));

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
