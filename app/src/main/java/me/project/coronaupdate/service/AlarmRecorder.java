package me.project.coronaupdate.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.util.logging.LogRecord;

import me.project.coronaupdate.R;

public class AlarmRecorder extends IntentService {
    public AlarmRecorder() {
        super("AlarmRecorder");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Handler handler = new Handler() ;
        Runnable periodicUpdate = new Runnable() {
            @Override
            public void run() {
                Log.i("recorder", "executeAlarm @ "+ SystemClock.elapsedRealtime());

                displayNotification(getApplicationContext(),"hahahah");
            }
        };
        handler.post(periodicUpdate);
        AlarmTrigger.completeWakefulIntent(intent);
    }

    private void displayNotification(Context context,String title) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "notify_001");

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText("Lokasi Terkini");
        // bigText.setBigContentTitle("1");
        bigText.setBigContentTitle(title);
        bigText.setSummaryText("Text in detail");

        //mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
        // mBuilder.setContentTitle("2");
        mBuilder.setContentTitle(title);
        mBuilder.setContentText("Your text");
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // === Removed some obsoletes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String channelId = "epatrol";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());
    }

}
