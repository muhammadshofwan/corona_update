package me.project.coronaupdate.feature.indonesia;

import android.app.Activity;

import androidx.lifecycle.ViewModel;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import me.project.coronaupdate.databinding.FragmentHomeBinding;

public class HomeViewModel extends ViewModel {


    int totalTerkonfirmasi = 0 ;
    int totalSembuh = 0;
    int totalMeninggal = 0;

   public void getData(Activity activity, final FragmentHomeBinding binding){

      AndroidNetworking.get("https://pomber.github.io/covid19/timeseries.json")
              .setTag("test")
              .setPriority(Priority.LOW)
              .build()
              .getAsJSONObject(new JSONObjectRequestListener() {
                 @Override
                 public void onResponse(JSONObject response) {
                    // do anything with response

                    try {
                    //   JSONObject jsonObject = response.getJSONObject("Indonesia");
                       JSONArray array = response.getJSONArray("Indonesia");

                        List<Integer> terkonfirmasiList = new ArrayList<Integer>();
                        List<Integer> sembuhList = new ArrayList<Integer>();
                       List<Integer> meninggalList = new ArrayList<Integer>();
                       for (int k = 0; k<array.length(); k++) {
                          JSONObject jsonObject = array.getJSONObject(k);

                          terkonfirmasiList.add(jsonObject.getInt("confirmed"));
                          sembuhList.add(jsonObject.getInt("recovered"));
                          meninggalList.add(jsonObject.getInt("deaths"));

                          findDuplicatesTerkonfirmasi(terkonfirmasiList,binding);
                          findDuplicatesSembuh(sembuhList,binding);
                          findDuplicatesMeninggal(meninggalList,binding);



                         // totalTerkonfirmasi += jsonObject.getInt("confirmed");
                         // totalSembuh += jsonObject.getInt("recovered");
                         // totalMeninggal += jsonObject.getInt("deaths");
                       }

                      // binding.lblTerkonfirmasi.setText(Integer.toString(totalTerkonfirmasi));
                      // binding.lblSembuh.setText(Integer.toString(totalSembuh));
                     //  binding.lblMeninggal.setText(Integer.toString(totalMeninggal));
                    } catch (JSONException e) {
                       e.printStackTrace();
                    }
                 }
                 @Override
                 public void onError(ANError error) {
                    // handle error
                 }
              });

   }

    public void findDuplicatesTerkonfirmasi(List<Integer> listContainingDuplicates,FragmentHomeBinding binding)
    {
        final Set<Integer> setToReturn = new HashSet<>();
        final Set<Integer> set1 = new HashSet<>();

        for (Integer yourInt : listContainingDuplicates)
        {
            if (!set1.add(yourInt))
            {
                setToReturn.add(yourInt);
            }
        }

        for (Integer data : setToReturn){
            totalTerkonfirmasi += data;
        }

        binding.lblTerkonfirmasi.setText(String.valueOf(totalTerkonfirmasi));

    }

    public void findDuplicatesSembuh(List<Integer> listContainingDuplicates,FragmentHomeBinding binding)
    {
        final Set<Integer> setToReturn = new HashSet<>();
        final Set<Integer> set1 = new HashSet<>();

        for (Integer yourInt : listContainingDuplicates)
        {
            if (!set1.add(yourInt))
            {
                setToReturn.add(yourInt);
            }
        }

        for (Integer data : setToReturn){
            totalSembuh += data;
        }

        binding.lblSembuh.setText(String.valueOf(totalSembuh));

    }

    public void findDuplicatesMeninggal(List<Integer> listContainingDuplicates,FragmentHomeBinding binding)
    {
        final Set<Integer> setToReturn = new HashSet<>();
        final Set<Integer> set1 = new HashSet<>();

        for (Integer yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }

        for (Integer data : setToReturn){
            totalMeninggal += data;
        }
        binding.lblMeninggal.setText(String.valueOf(totalMeninggal));

    }
}