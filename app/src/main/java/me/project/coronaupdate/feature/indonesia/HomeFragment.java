package me.project.coronaupdate.feature.indonesia;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.androidnetworking.AndroidNetworking;

import me.project.coronaupdate.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private DataIndoViewModel homeViewModel;

    private FragmentHomeBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(DataIndoViewModel.class);

        binding = binding.inflate(inflater, container, false);
        AndroidNetworking.initialize(getActivity().getApplicationContext());
        //set variables in Binding

        homeViewModel.getData(getActivity(),binding);
        return binding.getRoot();
    }
}
