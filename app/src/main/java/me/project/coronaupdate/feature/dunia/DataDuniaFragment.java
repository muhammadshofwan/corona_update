package me.project.coronaupdate.feature.dunia;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import me.project.coronaupdate.databinding.FragmentDataDuniaBinding;

public class DataDuniaFragment extends Fragment {

    private DataDuniaViewModel dataDuniaViewModel;
    private FragmentDataDuniaBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dataDuniaViewModel =
                ViewModelProviders.of(this).get(DataDuniaViewModel.class);
        binding = binding.inflate(inflater, container, false);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        binding.recycleView.setLayoutManager(llm);
        binding.recycleView.setHasFixedSize(false);
        dataDuniaViewModel.getData(getActivity(),binding);

        return binding.getRoot();
    }
}
