package me.project.coronaupdate.feature.indonesia;

import android.app.Activity;

import androidx.lifecycle.ViewModel;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.project.coronaupdate.databinding.FragmentHomeBinding;

public class DataIndoViewModel extends ViewModel {


    int totalTerkonfirmasi = 0 ;
    int totalSembuh = 0;
    int totalMeninggal = 0;

   public void getData(Activity activity, final FragmentHomeBinding binding){

      // String hostglobal = "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php";
       String host = "https://coronavirus-monitor.p.rapidapi.com/coronavirus/latest_stat_by_country.php?country=Indonesia";
      AndroidNetworking.get(host)
              .addHeaders("x-rapidapi-host","coronavirus-monitor.p.rapidapi.com")
              .addHeaders("x-rapidapi-key","c1e6487cbdmshb6da31c6502b998p1b6962jsn06bd14124c5a")
              .setTag("data")
              .setPriority(Priority.LOW)
              .build()
              .getAsJSONObject(new JSONObjectRequestListener() {
                 @Override
                 public void onResponse(JSONObject response) {
                    // do anything with response

                    try {
                       JSONArray array = response.getJSONArray("latest_stat_by_country");
                        JSONObject data = array.getJSONObject(0);

                        binding.lblTerkonfirmasi.setText(data.getString("total_cases"));
                        binding.lblSembuh.setText(data.getString("total_recovered"));
                        binding.lblMeninggal.setText(data.getString("total_deaths"));
                        binding.lblPerawatan.setText(data.getString("active_cases"));
                        binding.tanggalUpdate.setText(data.getString("record_date"));



                    } catch (JSONException e) {
                       e.printStackTrace();
                    }
                 }
                 @Override
                 public void onError(ANError error) {
                    // handle error
                 }
              });

   }


}