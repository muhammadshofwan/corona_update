package me.project.coronaupdate.feature.dunia;

import android.app.Activity;

import androidx.lifecycle.ViewModel;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import java.util.ArrayList;

import me.project.coronaupdate.adapter.DataDuniaAdapter;
import me.project.coronaupdate.databinding.FragmentDataDuniaBinding;
import me.project.coronaupdate.model.CountryStat;

public class DataDuniaViewModel extends ViewModel {

    public void getData(final Activity activity, final FragmentDataDuniaBinding binding){

        AndroidNetworking.initialize(activity);

        AndroidNetworking.get("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php")
                .addHeaders("x-rapidapi-host","coronavirus-monitor.p.rapidapi.com")
                .addHeaders("x-rapidapi-key","c1e6487cbdmshb6da31c6502b998p1b6962jsn06bd14124c5a")
                .setTag("list")
                .setPriority(Priority.LOW)
                .build()
                .getAsObject(CountryStat.class, new ParsedRequestListener<CountryStat>() {
                    @Override
                    public void onResponse(CountryStat respone) {

                        DataDuniaAdapter adapter = new DataDuniaAdapter(activity,respone.getCountryStatArrayList());
                        binding.recycleView.setAdapter(adapter);

                    }
                    @Override
                    public void onError(ANError anError) {
                        // handle error
                    }
                });
    }
}