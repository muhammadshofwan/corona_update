package me.project.coronaupdate.feature.detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

import me.project.coronaupdate.R;
import me.project.coronaupdate.databinding.ActivityDetailDataBinding;
import me.project.coronaupdate.model.CountryStat;

public class DetailDataActivity extends AppCompatActivity {

    ActivityDetailDataBinding binding;
    Activity activity = DetailDataActivity.this;

    CountryStat countryStat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity,R.layout.activity_detail_data);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){

            countryStat = (CountryStat) bundle.getSerializable("object");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setTitle(countryStat.getCountry_name());

        setData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setData(){
        binding.lblTerkonfirmasi.setText(countryStat.getCases());
        binding.lblPerawatan.setText(countryStat.getActive_cases());
        binding.lblSembuh.setText(countryStat.getTotal_recovered());
        binding.lblMeninggal.setText(countryStat.getDeaths());
        binding.lblNewCases.setText(countryStat.getNew_cases());
        binding.lblNewDeaths.setText(countryStat.getNew_deaths());
        binding.lblSeriousCritical.setText(countryStat.getSerious_critical());

    }
}
