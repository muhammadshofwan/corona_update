package me.project.coronaupdate.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import me.project.coronaupdate.R;
import me.project.coronaupdate.feature.detail.DetailDataActivity;
import me.project.coronaupdate.model.CountryStat;

public class DataDuniaAdapter extends RecyclerView.Adapter<DataDuniaAdapter.ViewHolder>  {
    private ArrayList<CountryStat> data;
    private Activity activity;

    public DataDuniaAdapter(Activity activity, ArrayList<CountryStat> data) {
        this.data=data;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_data_dunia, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        CountryStat item = new CountryStat();
        item = this.data.get(i);

        viewHolder.lbl_negara.setText(item.getCountry_name());
        viewHolder.lbl_cases.setText(item.getCases());
        viewHolder.lbl_recovered.setText(item.getTotal_recovered());
        viewHolder.lbl_death.setText(item.getDeaths());

        final CountryStat finalItem = item;
        viewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                HashMap<String,String> extras = new HashMap<>();
                extras.put("nama",finalItem.getNama());
                extras.put("tanggal",finalItem.getTanggal());
                extras.put("hari",finalItem.getHari());
                FastIntent.intentExtrasString(activity, DetailBarangActivity.class,extras); **/

                Intent intent = new Intent(activity,DetailDataActivity.class);
                intent.putExtra("object", (Serializable) finalItem);
                activity.startActivity(intent);

            }
        });

    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//
            }
        };
    }


    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }


    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView lbl_negara,lbl_cases,lbl_recovered,lbl_death;
        private View container;

        public ViewHolder(View view) {
            super(view);
            lbl_negara = (TextView) view.findViewById(R.id.lbl_country);
            lbl_cases= (TextView) view.findViewById(R.id.lbl_cases);
            lbl_recovered=view.findViewById(R.id.lbl_recovered);
            lbl_death=view.findViewById(R.id.lbl_deaths);
            container = view.findViewById(R.id.card_view);
        }
    }


}

